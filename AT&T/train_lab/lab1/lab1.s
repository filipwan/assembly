.bss
# Buffers
outputLine: .space 64

.data

# Constant values:
## strings:
welcomeMsg: .string "Witam\n\n"
welcomeMsgLen = . - welcomeMsg

## input data:
LAYER_NUMBER = 5

## ASCI codes:
ASCII_SPACE = ' '
ASCII_STAR = '*'
ASCII_NEW_LINE = '\n'

## System:
READ = 3
WRITE = 4
EXIT = 1

STDIN = 0
STDOUT = 1
EXIT_VALUE = 0

SYSTEM_CALL = 0x80


.text
.global _start
_start:
	mov $WRITE, %eax
	mov $STDOUT, %ebx
	mov $welcomeMsg, %ecx
	mov $welcomeMsgLen, %edx
	int $SYSTEM_CALL

	mov $0, %esi
newLineLoop:
	mov $0, %edi
	call prepareNewLine
	call writeLine
	inc %esi
	cmp $LAYER_NUMBER, %esi
	jne newLineLoop
	
exit:
	mov $EXIT, %eax
	mov $EXIT_VALUE, %ebx
	int $SYSTEM_CALL

prepareNewLine:
	movb $'_', outputLine(%edi)
	inc %edi
	cmp $LAYER_NUMBER, %edi
	jne prepareNewLine
	mov $LAYER_NUMBER, %ecx
	add %esi, %ecx
	inc %ecx

prepareNewLineStars:
	movb $'*', outputLine(%edi)
	inc %ecx
	cmp %ecx, %edi
	jne prepareNewLineStars
	inc %edi
	movb $'\n', outputLine(%edi)

	ret

writeLine:
	mov $WRITE, %eax
	mov $STDOUT, %ebx
	mov $outputLine, %ecx
	mov $64, %edx
	int $SYSTEM_CALL
	jmp exit
	ret






