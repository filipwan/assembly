# Number of kernel functions
EXIT_NR = 1
READ_NR = 3
WRITE_NR = 4

SYS_CALL = 0x80

# Definitions
STDIN = 0
STDOUT = 1
EXIT_SUCCESS = 0

.section .bss
inputFirst: .space 64
inputSecond: .space 64

.section .data


.section .text
.globl main
main:


	pushl $0
	call exit
