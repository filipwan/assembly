.bss

.lcomm dxIntegral, 4

.lcomm inputStart, 4
.lcomm inputEnd, 4

.data

sIntegral: .float 0
iIntegral: .long 0
rectangleCount: .long 4000		# Ilosc prostokatow, na ktore zostanie podzielona funkcja

inputFormatNumber: .string "%f"
outputFormatNumber: .string "%f\n"
formatText: .string "%s\n"
inputTextStart: .string "Prosze podac poczatek przedzialu calkowania:"
inputTextEnd: .string "Prosze podac koniec przedzialu calkowania:"
outputText:	.string "Wynikiem calki jest: "

READ = 3
WRITE = 4
EXIT = 1

STD_IN = 0
STD_OUT = 1
EXIT_VALUE = 0

SYSTEM_CALL = 0X80

.text
.globl main
main:
	pushl	$inputTextStart
	pushl	$formatText
	call	printf
	addl	$8, %esp
	
	pushl	$inputStart
	pushl	$inputFormatNumber
	call	scanf
	addl	$8, %esp

	pushl	$inputTextEnd
	pushl	$formatText
	call	printf
	addl	$8, %esp

	pushl	$inputEnd
	pushl 	$inputFormatNumber
	call	scanf
	addl	$8, %esp
	
	call integral

	pushl	$outputText
	pushl	$formatText
	call	printf
	addl	$8, %esp
	
	subl	$8, %esp
	flds	sIntegral
	fstpl	(%esp)
	pushl	$outputFormatNumber
	call	printf
	addl	$8, %esp	

exit:
	movl	$EXIT, %eax
	movl	$EXIT_VALUE, %ebx
	int 	$SYSTEM_CALL

integral:
	flds	inputStart
	flds	inputEnd
	fsubp
	fild	rectangleCount	
	fdivrp
	fstps	dxIntegral
	
	xor 	%ecx, %ecx
	
integral_loop:
	inc 	%ecx
	
	movl	%ecx, iIntegral
	
	flds	dxIntegral
	fild	iIntegral
	fmulp
	flds	inputStart
	faddp	
	fsin
	flds	sIntegral
	faddp	
	fstps	sIntegral

	cmpl 	rectangleCount, %ecx
	jb		integral_loop
	
	flds	sIntegral
	flds	dxIntegral
	fmulp
	fstps	sIntegral
ret


.type my_sin, @function
my_sin:
	pushl	%ebp			# Zapisz poprzedni wskaznik bazowy
	movl	%esp, %ebp		# Ustaw nowy wskaznik bazowy dla funkcji
	subl	$4, %esp
	movl	%eax, -4(%ebp)

	flds	-4(%ebp)
	fsin
	fstps	-4(%ebp)
	
	movl	-4(%ebp), %eax
	movl	%ebp, %esp
	popl	%ebp

ret
