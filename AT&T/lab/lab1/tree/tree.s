# Numery funkcji systemowych na podstawie pliku asm/unistd.h
SYSEXIT 		= 1
SYSREAD			= 3
SYSWRITE		= 4
STDOUT			= 1
SYSCALL32		= 0x80
EXIT_SUCCESS	= 0

.bss	# Sekcja buforów
output: .space 64	# Zarezerwowanie bufora 64 bajtów
outputLen = 64		# Długość bufora

.data	# Sekcja danych
LINE_AMMOUNT = 5	# Ustalona ilość wypisanych poziomów drzewa

.text	# Sekcja kodu
.global _start
_start:
# Inicjalizacja liczników linii %esi i %edi
	mov $LINE_AMMOUNT, %esi
	dec %esi
	mov %esi, %edi

# Początek pętli wypisywania kolejnych linii choinki
loopLine:
	xor %ecx, %ecx
	jmp loopSpaces
loopLineCont:
	inc %edi
	dec %esi
	cmp $0, %esi
	jge writeLine
	jmp finalize

# Pętla umieszczająca odpowiednią ilość spacji w buforze
loopSpaces:
	cmp %esi, %ecx
	jge loopStars
	movb $' ', output(%ecx)
	inc %ecx
	jmp loopSpaces

# Pętla umieszczająca odpowiednią ilość gwiazdek w buforze
loopStars:
	movb $'*', output(%ecx)
	inc %ecx
	cmp %edi, %ecx
	jbe loopStars
	movb $'\n', output(%ecx)
	jmp loopLineCont

# Wypisanie linii znajdującej się w buforze
writeLine:
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $output, %ecx
	mov $outputLen, %edx
	int $SYSCALL32
	jmp loopLine

# Wypisanie ostatniego poziomu choinki
finalize:
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $output, %ecx
	mov $outputLen, %edx
	int $SYSCALL32

	xor %ecx, %ecx	
	movb $'\n', output(%edi)

# Wypisanie pnia drzewa
trunkLoop:
	dec %edi
	movb $' ', output(%edi)
	cmp $0, %edi
	jg trunkLoop
	mov $LINE_AMMOUNT, %edi
	dec %edi
	movb $'*', output(%edi)

	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $output, %ecx
	mov $outputLen, %edx
	int $SYSCALL32

# Zakończenie programu
exit:
	mov $SYSEXIT, %eax
	mov $EXIT_SUCCESS, %ebx
	int $SYSCALL32



