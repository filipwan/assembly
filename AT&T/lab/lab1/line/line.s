# Numery funkcji systemowych na podstawie pliku asm/unistd.h
SYSEXIT 		= 1
SYSREAD			= 3
SYSWRITE		= 4
STDOUT			= 1
SYSCALL32		= 0x80
EXIT_SUCCESS	= 0

.bss	# Sekcja buforów
output: .space 64	# Zarezerwowanie bufora 64 bajtów
outputLen = 64		# Długość bufora

.data	# Sekcja danych
STAR_AMMOUNT = 10	# Ustalona ilość wypisanych gwiazdek

.text	# Sekcja kodu
.global _start
_start:
	xor %ecx, %ecx	# wyzerowanie rejestru %ecx

# Pętla zapisującą zadaną ilość gwiazdek do bufora
loopStars:
	movb $'*', output(%ecx)
	inc %ecx
	cmp $STAR_AMMOUNT, %ecx
	jb loopStars
	movb $'\n', output(%ecx)
	
# Wypisanie uzyskanego ciągu znaków
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $output, %ecx
	mov $outputLen, %edx
	int $SYSCALL32

# Zakończenie programu
	mov $SYSEXIT, %eax
	mov $EXIT_SUCCESS, %ebx
	int $SYSCALL32



