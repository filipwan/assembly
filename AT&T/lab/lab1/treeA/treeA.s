.bss
output: .space 64
input: .space 3

.data
LINE_AMMOUNT = 5

.text
.global _start
_start:
	mov $3, %eax
	mov $0, %ebx
	mov $input, %ecx
	mov $3, %edx
	int $0x80


parse:
	add input(%ebx), %esi
	sub $'0', %esi
	inc %ebx
	movb input(%ebx), %cl
	cmp $10, %cl
	je makeTree
	imull $10, %esi
	add input(%ebx), %esi
	sub $'0', %esi
	push %esi

makeTree:
	mov (%esp), %esi
	dec %esi
	mov %esi, %edi

loopLine:
	mov $0, %ecx
	jmp loopSpaces
loopLineCont:
	inc %edi
	dec %esi
	cmp $0, %esi
	jge writeLine
	jmp finalize

loopSpaces:
	cmp %esi, %ecx
	jge loopStars
	movb $' ', output(%ecx)
	inc %ecx
	jmp loopSpaces

loopStars:
	movb $'*', output(%ecx)
	inc %ecx
	cmp %edi, %ecx
	jbe loopStars
	movb $'\n', output(%ecx)
	jmp loopLineCont

writeLine:
	mov $4, %eax
	mov $1, %ebx
	mov $output, %ecx
	mov $64, %edx
	int $0x80
	jmp loopLine

finalize:
	mov $4, %eax
	mov $1, %ebx
	mov $output, %ecx
	mov $64, %edx
	int $0x80

	mov $0, %ecx	

	
	movb $'\n', output(%edi)
trunkLoop:
	dec %edi
	movb $' ', output(%edi)
	cmp $0, %edi
	jg trunkLoop
	mov (%esp), %edi
	dec %edi
	movb $'*', output(%edi)
	mov $4, %eax
	mov $1, %ebx
	mov $output, %ecx
	mov $64, %edx
	int $0x80

exit:
	mov $1, %eax
	mov $0, %ebx
	int $0x80



