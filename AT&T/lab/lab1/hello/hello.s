# Numery funkcji systemowych na podstawie pliku asm/unistd.h
SYSEXIT 		= 1
SYSREAD			= 3
SYSWRITE		= 4
STDOUT			= 1
SYSCALL32		= 0x80
EXIT_SUCCESS	= 0

.data
# Deklaracja zmiennych zawierających napis do wyświetlenia oraz jego długość
output:	.string "Hello World!\n"
outputLen = . - output

.text
.global _start
_start:
	# Wypisanie napisu output o długości outputLen na strumień wyjściowy STDOUT
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	mov $output, %ecx
	mov $outputLen, %edx
	int $SYSCALL32

	mov $SYSEXIT, %eax
	mov $EXIT_SUCCESS, %ebx
	int $SYSCALL32



