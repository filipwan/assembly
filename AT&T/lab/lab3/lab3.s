# Numery funkcji systemowych
SYS_EXIT	 = 1
SYS_READ	 = 3
SYS_WRITE	 = 4
STD_IN		 = 0
STD_OUT		 = 1
SYS_CALL_32	 = 0x80
EXIT_SUCCESS = 0

#----------------------------------------
.bss

#----------------------------------------
.data

liczba1: .float 15.1
liczba2: .float 5.2
floatExponentValue: .byte 0x7F

#----------------------------------------
.text
.globl main
main:
	xor %ecx, %ecx
	xor %esi, %esi

	movl liczba1, %eax
	movl liczba2, %edx
	ror $23, %eax		# Shift eax and edx to right 23 bits
	ror $23, %edx		# Now exponents are in al and dl
	cmpb %dl, %al
	je main_equalExponents
	ja main_firstIsBigger
	xchg %edx, %eax		# Exchange eax and edx, so eax exponent is bigger than edx
main_firstIsBigger:
	movb %al, %cl
	subb %dl, %cl
	addb %cl, %dl		# Set edx exponent to eax exponent
	rol $23, %edx
	shrd $23, %edx, %esi
	ror $23, %edx
	#clc					# Set carry flag to 1
	#rcr $1, %esi
	#dec %cl
	#cmpb $0, %cl
	#je main_cliszero
	shr %cl, %esi
main_cliszero:
	shld $23, %esi, %edx
	ror $23, %edx

main_equalExponents:
	rol $23, %eax
	rol $23, %edx
	mov %eax, %esi
	# mamy takie same wykladniki
	
	call addSignificant
	movl %esi, %edx
	ror $23, %edx
	jnc main_notCarry
	inc %dl
	
main_notCarry:
	rol $9, %eax
	shld $23, %eax, %edx
	nop

exit:
	mov $SYS_EXIT, %eax
	mov $EXIT_SUCCESS, %ebx
	int $SYS_CALL_32
#-----------------------------------------

addSignificant:	# Adds %edx to %eax
	shl $9, %eax		# Shift eax and edx to left 9 bits
	shl $9, %edx
	
	clc					# Clear carry flag
	adc %edx, %eax		# Add edx to eax with carry
	jnc addSignificant_continue	# Jump if carry flag is 0
	rcr $10, %eax		# Rotate eax right 10 bit with flag
	stc					# Set carry flag to 1
	xor %edx, %edx
 ret
addSignificant_continue:
	ror $9, %eax		# Rotate eax right 9 bits
	xor %edx, %edx
ret
#----------------------------------------



