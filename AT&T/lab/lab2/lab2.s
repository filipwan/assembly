# Numery funkcji systemowych
SYSEXIT		 = 1
SYSREAD		 = 3
SYSWRITE	 = 4
STDIN		 = 0
STDOUT		 = 1
SYSCALL32 	 = 0x80
EXIT_SUCCESS = 0
BUFFOR_LEN	 = 64
#----------------------------------------

.bss
inputFirst: .space 64
inputSecond: .space 64
output: .space 65
loadedFirstLen: .byte 0
loadedSecondLen: .byte 0
outputLen: .byte 0
outputLoadedLen: .byte 0
#----------------------------------------

.data
msgFirst: .string "Podaj 1 liczbe: \n"
msgFirstLen = . - msgFirst
msgSecond: .string "Podaj 2 liczbe: \n"
msgSecondLen = . - msgSecond
msgResult: .string "Wynik to: \n"
msgResultLen = . - msgResult
msgEndl: .string "\n\n"
msgEndlLen = . - msgEndl

achar: .byte 'a'
#----------------------------------------

.text
.globl main
main:
	call loadNumbers
	call inputToHex

	movb loadedFirstLen, %cl
	movb %cl, outputLoadedLen
	inc %cl
	movb %cl, outputLen

	xor %ecx, %ecx
	xor %edx, %edx
	clc	# clear carry flag

loopAddition:
	call loadNextNumberPart
	
	adcl %edx, %eax
	
	call loadResultToOutput

	movb loadedFirstLen, %cl
	cmpb $0, %cl
	jne loopAddition

	call outputToChar
	mov $msgResult, %ecx
	mov $msgResultLen, %edx
	call write
	mov $output, %ecx
	mov outputLen, %edx
	call write
	mov $msgEndl, %ecx
	mov $msgEndlLen, %edx
	call write
	
	nop

	
exit:
	mov $SYSEXIT, %eax
	mov $EXIT_SUCCESS, %ebx
	int $SYSCALL32
#-----------------------------------------
loadNumbers:	# Wczytuje input do buforow
	mov $msgFirst, %ecx
	mov $msgFirstLen, %edx
	call write
	
	mov $inputFirst, %ecx
	mov $BUFFOR_LEN, %edx
	call read
	dec %eax
	movb %al, loadedFirstLen
	
	mov $msgSecond, %ecx
	mov $msgSecondLen, %edx
	call write
	
	mov $inputSecond, %ecx
	mov $BUFFOR_LEN, %edx
	call read
	dec %eax
	movb %al, loadedSecondLen
ret
#-----------------------------------------
write:		# Wypisuje string %ecx o %edx znakach
	mov $SYSWRITE, %eax
	mov $STDOUT, %ebx
	int $SYSCALL32
ret
#-----------------------------------------	
read:		# Wczytuje do bufora w %ecx string o %edx znakach
	mov $SYSREAD, %eax
	mov $STDIN, %ebx
	int $SYSCALL32
ret
#-----------------------------------------
inputToHex:	# Zamienia w input char na hex
	xor %ecx, %ecx
	movb loadedFirstLen, %cl
 inputToHex_First:
	dec %cl
	movb inputFirst(,%ecx,1), %al
	call charToHex
	movb %al, inputFirst(,%ecx,1)
	cmpb $0, %cl
	jne inputToHex_First
	
	movb loadedSecondLen, %cl
 inputToHex_Second:
	dec %cl
	movb inputSecond(,%ecx,1), %al
	call charToHex
	movb %al, inputSecond(,%ecx,1)
	cmpb $0, %cl
	jne inputToHex_Second
ret
#----------------------------------------
charToHex:	# Zamienia char w %al na hex
	cmpb $'a', %al
	jb charToHex_Cap
	subb $'a', %al
	addb $10, %al
 ret
 charToHex_Cap:
	cmpb $'A', %al 
	jb charToHex_Num
	subb $'A', %al
	addb $10, %al
 ret
 charToHex_Num:
	sub $'0', %al
ret
#----------------------------------------
loadNextNumberPart:		# Wczytuje nastepne slowo do %edx i %eax
	xor %eax, %eax
	xor %edx, %edx
	movb loadedFirstLen, %cl
	cmp $4, %cl
	jbe loadNextNumberPart_First_LastWord
	cmp $8, %cl
	jb loadNextNumberPart_First_LastDBWord

	sub $4, %cl
	movl inputFirst(,%ecx,1), %eax
	sub $4, %cl
	movl inputFirst(,%ecx,1), %edx
	movb %cl, loadedFirstLen
	call bytesToHex
	movl %eax, %ebx

 loadNextNumberPart_Second:
	xor %eax, %eax
	xor %edx, %edx
	movb loadedSecondLen, %cl
	cmp $4, %cl
	jbe loadNextNumberPart_Second_LastWord
	cmp $8, %cl
	jb loadNextNumberPart_Second_LastDBWord

	sub $4, %cl
	movl inputSecond(,%ecx,1), %eax
	sub $4, %cl
	movl inputSecond(,%ecx,1), %edx
	movb %cl, loadedSecondLen
	call bytesToHex
	movl %ebx, %edx
	xor %ebx, %ebx
 ret
 loadNextNumberPart_First_LastWord:
	dec %cl
	rol $8, %eax
	movb inputFirst(,%ecx,1), %al
	cmp $0, %cl
	jne loadNextNumberPart_First_LastWord

	xor %edx, %edx
	movb %cl, loadedFirstLen
	call bytesToHex
	movl %eax, %ebx

 jmp loadNextNumberPart_Second
 loadNextNumberPart_First_LastDBWord:
	movl %ecx, %edi
	movb $4, %cl
 loadNextNumberPart_First_LastDBWord_Loop1:
	dec %edi
	rol $8, %eax
	movb inputFirst(,%edi,1), %al
	loop loadNextNumberPart_First_LastDBWord_Loop1

	mov %edi, %ecx
	xor %edi, %edi
 loadNextNumberPart_First_LastDBWord_Loop2:
	dec %cl
	rol $8, %edx
	movb inputFirst(,%ecx,1), %dl
	cmpb $0, %cl
	jne loadNextNumberPart_First_LastDBWord_Loop2

	call bytesToHex
	movl %eax, %ebx

 jmp loadNextNumberPart_Second
 loadNextNumberPart_Second_LastWord:
	dec %cl
	rol $8, %eax
	movb inputSecond(,%ecx,1), %al
	cmp $0, %cl
	jne loadNextNumberPart_Second_LastWord

	xor %edx, %edx
	movb %cl, loadedSecondLen
	call bytesToHex
	mov %ebx, %edx
	xor %ebx, %ebx

 ret
 loadNextNumberPart_Second_LastDBWord:
	movl %ecx, %edi
	movb $4, %cl
 loadNextNumberPart_Second_LastDBWord_Loop1:
	dec %edi
	rol $8, %eax
	movb inputSecond(,%edi,1), %al
	loop loadNextNumberPart_Second_LastDBWord_Loop1

	mov %edi, %ecx
	xor %edi, %edi
 loadNextNumberPart_Second_LastDBWord_Loop2:
	dec %cl
	rol $8, %edx
	movb inputSecond(,%ecx,1), %dl
	cmpb $0, %cl
	jne loadNextNumberPart_Second_LastDBWord_Loop2

	call bytesToHex
	mov %ebx, %edx
	xor %ebx, %ebx

 loadNextNumberPart_Return:
ret
#----------------------------------------
outputToChar:
	movb outputLen, %cl
	movb $'\n', output(,%ecx,1)
	dec %cl
 outputToChar_Loop:
	movb output(,%ecx,1), %al
	call hexToChar
	movb %al, output(,%ecx,1)
	loop outputToChar_Loop
	xor %edx, %edx
	#mov $'1', %dl
	#mov $' ', %eax
	#cmovc %edx, %eax
	#movb %al, output(,%ecx,1)
	
	xor %eax, %eax
ret
#-----------------------------------------
hexToChar:	# Zamienia wartosc hex w %al na char
	cmpb $0xa, %al
	jb hexToChar_Num
	addb $'a', %al
	subb $10, %al
 ret
 hexToChar_Num:
	addb $'0', %al
ret
#-----------------------------------------
		# Zamienia 2 slowa pojedynczych wartosci w kazdym bajcie w %edx:%eax
bytesToHex:	# na 1 slowo pelnej wartosci w %eax
	pushl %ebx
	xor %ebx, %ebx
	mov $4, %cl
 bytesToHex_FirstPart:
	add %dl, %bl
	ror $8, %edx
	rol $4, %ebx
	loop bytesToHex_FirstPart
	
	mov $4, %cl
 bytesToHex_SecondPart:
	add %al, %bl
	ror $8, %eax
	rol $4, %ebx
	loop bytesToHex_SecondPart
	rol $28, %ebx	
	
	mov %ebx, %eax
	popl %ebx
	xor %edx, %edx
ret
#------------------------------------------
loadResultToOutput:
	call hexToBytes
	movb outputLoadedLen, %cl
	#dec %cl
	cmpb $4, %cl
	jbe loadResultToOutput_Word
	cmpb $8, %cl
	jb loadResultToOutput_DBWord

	inc %cl
	sub $4, %cl
	movl %eax, output(,%ecx,1)
	sub $4, %cl
	movl %edx, output(,%ecx,1)
 ret
 loadResultToOutput_Word:
	inc %cl
  loadResultToOutput_Word_Loop:
	movb %al, output(,%ecx,1)
	rol $8, %eax
	dec %cl
	cmpb $0, %cl
	jne loadResultToOutput_Word_Loop
	
	mov $output, %edi
 ret
 loadResultToOutput_DBWord:
	inc %cl
	sub $4, %cl
	movl %eax, output(,%ecx,1)
	inc %cl

  loadResultToOutput_DBWord_Loop:
	movb %dl, output(,%ecx,1)
	rol $8, %edx
	dec %cl
	cmpb $0, %cl
	jne loadResultToOutput_DBWord_Loop

ret
#------------------------------------------
		# Zamienia 1 slowo pelnej wartosci %eax
hexToBytes:	# Na dwa slowa pojedynczych wartosci w kazdym bajcie w %edx:%eax
	mov %eax, %ebx
	xor %eax, %eax
	xor %edx, %edx
	
	movl $4, %ecx
 hexToBytes_First:
	shld $4, %ebx, %edx
	ror $12, %edx
	rol $4, %ebx
	loop hexToBytes_First
	rol $4, %edx
	
	
	movl $4, %ecx
 hexToBytes_Second:
	shld $4, %ebx, %eax
	ror $12, %eax
	rol $4, %ebx
	loop hexToBytes_Second
	rol $4, %eax

	xor %ebx, %ebx
ret
#-----------------------------------------
