.text
.global _start
_start:
	mov $3, %eax
	mov $0, %ebx
	mov $input, %ecx
	mov $256, %edx
	int $0x80
	
	mov $0, %ecx
textSwapIteration:
	movb input(,%ecx,1), %ah
	
	CMP $'z', %ah 
	ja textSwapIncrement
	CMP $'a', %ah 
	jb textSwapUppercase
	SUB $32, %ah
	ja textSwapIncrement

textSwapUppercase:
	CMP $'A', %ah 
	jb textSwapIncrement
	CMP $'Z', %ah 
	ja textSwapIncrement
	ADD $32, %ah
	
textSwapIncrement:
	movb %ah, output(,%ecx,1)
	inc %ecx
	CMP $255, %ecx
	jbe textSwapIteration
	movb $0, output(,%ecx,1)
	
	mov $4, %eax
	mov $1, %ebx
	mov $output, %ecx
	mov $256, %edx
	int $0x80
	
	mov $1, %eax
	mov $0, %ebx
	int $0x80

.data
.comm input, 256
.comm output, 256

