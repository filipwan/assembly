ORG 0

start:
    ACALL delay_1s

    SJMP $

;------------------------------
copy_w_w:
    MOV A, R2
    JZ end_w_w

copy_w_w_1:
    MOV A, @R0
    MOV @R1, A

    INC R0
    INC R1
    DJNZ R2, copy_w_w_1
end_w_w:
    RET
;------------------------------
delay_1ms: 
    MOV R2, #250D
delay_1ms_1:
    NOP
    DJNZ R2, delay_1ms_1
    RET

;------------------------------
delay_1s:
    MOV R1, #250D
delay_1s_1:
    ACALL delay_1ms
    ACALL delay_1ms
    ACALL delay_1ms
    ACALL delay_1ms
    DJNZ R1, delay_1s_1
    RET
;------------------------------
END